//
//  ICesaro.cs
//
//  Author:
//       Willem Van Onsem <vanonsem.willem@gmail.com>
//
//  Copyright (c) 2014 Willem Van Onsem
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using NUtils;
using System.Collections.Generic;

namespace CesaroLimit {
	/// <summary>
	/// An interface specifying the object handles a Cesaro object. Cesaro algebra handles
	/// items (called "indices").
	/// </summary>
	/// <typeparam name='TIndex'>
	/// The type of the indices in the Cesaro structure.
	/// </typeparam>
	public interface ICesaro<TIndex> {

		/// <summary>
		/// Determines whether the two given indices are communicating.
		/// </summary>
		/// <returns><c>true</c> if <paramref name="index1"/> and <paramref name="index2"/> are communicating; otherwise, <c>false</c>.</returns>
		/// <param name="index1">The first index.</param>
		/// <param name="index2">The second index.</param>
		bool Communicate (TIndex index1, TIndex index2);

		/// <summary>
		/// Determines whether the given index is essential in the Cesaro structure.
		/// </summary>
		/// <returns><c>true</c> if the given <paramref name="index"/> is essential in the Cesaro structure; otherwise, <c>false</c>.</returns>
		/// <param name="index">The given index to check for.</param>
		bool IsEssential (TIndex index);

		/// <summary>
		/// Determines which indices in the Cesaro structure are essential.
		/// </summary>
		/// <returns><c>true</c> if the given <paramref name="index"/> is essential in the Cesaro structure; otherwise, <c>false</c>.</returns>
		IEnumerable<TIndex> EssentialIndices ();

		/// <summary>
		/// Determines whether the given index is not essential (or "transient") in the Cesaro structure.
		/// </summary>
		/// <returns><c>true</c> if the given <paramref name="index"/> is inessential (or "transient") in the Cesaro structure; otherwise, <c>false</c>.</returns>
		/// <param name="index">The given index to check for.</param>
		bool IsInessential (TIndex index);

		/// <summary>
		/// Gets a list of essential groups of indices.
		/// </summary>
		/// <returns>A list of lists that represent a group of essential indices that only communicate with each other.</returns>
		/// <remarks>
		/// <para>An essential group is a group of essential indices that communicate only with each other.</para>
		/// </remarks>
		IEnumerable<IEnumerable<TIndex>> GetEssentialGroups ();

		/// <summary>
		/// Gets a list of inessential self-communicating groups of indices.
		/// </summary>
		/// <returns>A list of lists that represent a group of inessential indices that communicate with each other (and potentially with essential indices as well).</returns>
		/// <remarks>
		/// <para>An inessential self-communicating group is a group of inessential indices that communicate with each other and potentially with
		/// essential indices as well.</para>
		/// </remarks>
		IEnumerable<IEnumerable<TIndex>> GetInEssentialSelfCommunicatingGroups ();

		/// <summary>
		/// Gets a list of inessential non self-communicating groups of indices.
		/// </summary>
		/// <returns>A list of inessential indices that does not communicate with any index (including itself).</returns>
		/// <remarks>
		/// <para>An inessential non self-communicating group is an index that communicates with no index (including itself).</para>
		/// <para>Since inessential non self-communicating groups are always singletons, we return a list of these indices.</para>
		/// </remarks>
		IEnumerable<TIndex> GetInEssentialNonSelfCommunicatingGroups ();

		/// <summary>
		/// Get the indices of elements that communicate with the given <paramref name="index"/>.
		/// </summary>
		/// <returns>A list of indices with which the given <paramref name="index"/> communicates.</returns>
		/// <param name="index">The given index to look for friends.</param>
		IEnumerable<TIndex> GetEssentialFriends (TIndex index);
	}
}

