//
//  ICesaroPermutation.cs
//
//  Author:
//       Willem Van Onsem <vanonsem.willem@gmail.com>
//
//  Copyright (c) 2014 Willem Van Onsem
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using NUtils.Maths;

namespace CesaroLimit {
	/// <summary>
	/// An interface that specifies that the instance is a <see cref="T:ICesaro`1"/> structure
	/// with <see cref="int"/> as indices that can generate a <see cref="IPermutation"/> that specifies
	/// how a <see cref="T:ICesaro`1"/> instance should be swapped such that it is ordered according to a Cesaro matrix.
	/// </summary>
	public interface ICesaroPermutation : ICesaro<int> {

		/// <summary>
		/// Generate a permutation that will swap the cesaro object in the correct order.
		/// </summary>
		/// <returns>A <see cref="IPermutation"/> instance that specifies how a <see cref="T:ICesaroMatrix`1"/> can be swapped
		/// such that the result is a Cesaro structure in the correct order.</returns>
		IPermutation GetCesaroPermutation ();
	}
}

