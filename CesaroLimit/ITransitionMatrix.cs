//
//  ITransitionMatrix.cs
//
//  Author:
//       Willem Van Onsem <vanonsem.willem@gmail.com>
//
//  Copyright (c) 2014 Willem Van Onsem
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System.Collections.Generic;
using NUtils.Maths;
using NUtils.Bitwise;

namespace CesaroLimit {
	/// <summary>
	/// An interface to represents whether an index <c>i</c> allows a transition to index <c>j</c>.
	/// </summary>
	public interface ITransitionMatrix : IPermutable, ICesaroPermutation {

		/// <summary>
		/// Checks if a transition from <c>i</c> to <c>j</c> is possible.
		/// </summary>
		/// <param name="i">The index from which the transition takes place.</param>
		/// <param name="j">The index to which the transition takes place.</param>
		bool this [int i, int j] {
			get;
		}

		/// <summary>
		/// Transforms this matrix into a <see cref="ITransitionMatrix"/> that specifies if there exists
		/// an eventual transition from <c>i</c> to <c>j</c>.
		/// </summary>
		/// <remarks>
		/// <para>
		/// An eventual transition is a path from <c>i</c> to <c>j</c> with arbitrary length (such path always has
		/// an upper bound of <c>n</c> with <c>n</c> the number of considered indices).
		/// </para>
		/// </remarks>
		void Boost ();

		/// <summary>
		/// Transforms this matrix into a <see cref="ITransitionMatrix"/> where the <c>i,j</c>-th element is one
		/// if there is a path from <c>i</c> to <c>j</c> or if there exists a <c>k</c> such that there is a path
		/// from <c>i</c> to <c>j</c> and from <c>j</c> to <c>k</c>.
		/// </summary>
		/// <remarks>
		/// <para>Note that performing this task O(log n) times, will result in a matrix that shows full connectivity. This
		/// is mainly the intention of the <see cref="Boost"/> method.</para>
		/// </remarks>
		void BoostSingle ();

		/// <summary>
		/// Calculates a <see cref="IBitVector"/> where all bits are set of the essential indices.
		/// </summary>
		/// <returns>A <see cref="IBitVector"/> where all the bits are set of the essential indices.</returns>
		/// <remarks>
		/// <para>By processing essential indices in batch, one yields a performance gain.</para>
		/// </remarks>
		IBitVector EssentialIndicesVector ();

		/// <summary>
		/// Calculates a <see cref="IBitVector"/> where all bits are set of the non-communicating indices.
		/// </summary>
		/// <returns>A <see cref="IBitVector"/> where all the bits are set of the non-communicating indices.</returns>
		/// <remarks>
		/// <para>By processing non-communicating indices in batch, one yields a performance gain.</para>
		/// </remarks>
		IBitVector NonCommunicatingIndicesVector ();

		/// <summary>
		/// Get the indices of elements that communicate with the given <paramref name="index"/>.
		/// </summary>
		/// <returns>A list of indices with which the given <paramref name="index"/> communicates.</returns>
		/// <param name="index">The given index to look for friends.</param>
		/// <param name="frm">An optional parameter that specifies the index from which essential friend
		/// detection begins. This is usefull to optimize essential group detection.</param>
		/// <remarks>
		/// <para>By default, the essential friend detection starts at index zero (<c>0</c>).</para>
		/// </remarks>
		IBitVector GetEssentialFriends (int index, int frm = 0x00);
	}
}

