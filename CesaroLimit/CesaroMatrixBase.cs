//
//  CesaroMatrixBase.cs
//
//  Author:
//       Willem Van Onsem <vanonsem.willem@gmail.com>
//
//  Copyright (c) 2014 Willem Van Onsem
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using NUtils.Maths;
using System;
using System.Collections.Generic;

namespace CesaroLimit {
	/// <summary>
	/// A basic generic implementation of the <see cref="T:ICesaroMatrix`1"/> interface. This basic implementation mainly
	/// handles operations like swapping etc.
	/// </summary>
	/// <typeparam name='TElement'>
	/// The type of elements stored in the CesaroMatrix. These elements should have an order relation together
	/// with a zero-element.
	/// </typeparam>
	public abstract class CesaroMatrixBase<TElement> : ICesaroMatrix<TElement> where TElement : IComparable<TElement> {

		private readonly int n;

		/// <summary>
		/// Gets the internally stored data.
		/// </summary>
		/// <value>The internally stored data.</value>
		protected abstract TElement[] Data {
			get;
		}

		/// <summary>
		/// Gets the element at the given index of the matrix.
		/// </summary>
		/// <param name="index">The given index.</param>
		public TElement this [int index] {
			get {
				return this.Data [index];
			}
		}

		/// <summary>
		/// Gets the element at the given row and column of the matrix.
		/// </summary>
		/// <param name="row">The given row.</param>
		/// <param name="column">The given column.</param>
		public TElement this [int row, int column] {
			get {
				return this.Data [row * this.Length + column];
			}
		}
		#region ILength implementation
		/// <summary>
		/// Gets the number of subelements.
		/// </summary>
		/// <value>The length.</value>
		public int Length {
			get {
				return this.n;
			}
		}
		#endregion
		/// <summary>
		/// Initializes a new instance of the <see cref="T:CesaroMatrixBase`1"/> class with a given number of indices.
		/// </summary>
		/// <param name="n">The number of indices of the matrix.</param>
		protected CesaroMatrixBase (int n) {
			this.n = n;
		}
		#region ICesaro implementation
		/// <summary>
		/// Checks if index <paramref name="i"/> communicates with <paramref name="j"/>.
		/// </summary>
		/// <param name="i">The first given index to check for.</param>
		/// <param name="j">The second given index to check for.</param>
		/// <returns><c>true</c> if index <paramref name="i"/> communicates with index <paramref name="j"/>, otherwise
		/// <c>false</c>.</returns>
		/// <remarks>
		/// <para>An index <paramref name="i"/> communicates with <paramref name="j"/> if the probability of
		/// transition from <paramref name="i"/> to <paramref name="j"/> and the probability of transition from
		/// <paramref name="j"/> to <paramref name="i"/> are both larger than zero.</para>
		/// </remarks>
		public abstract bool Communicate (int i, int j);

		/// <summary>
		/// Checks if one can reach index <paramref name="j"/> from index <paramref name="i"/>, in other
		/// words whether there is a probabilistic path from <paramref name="i"/> to <paramref name="j"/>.
		/// </summary>
		/// <param name="i">The initial index.</param>
		/// <param name="j">The target index.</param>
		public abstract bool Path (int i, int j);

		/// <summary>
		/// Determines whether the given index is essential.
		/// </summary>
		/// <returns><c>true</c> if the given index is essential; otherwise, <c>false</c>.</returns>
		/// <param name="i">The given index to check for.</param>
		/// <remarks>
		/// <para>An index <c>i</c> is essential if there is at least one index <c>j</c> such that the
		/// transition from <c>i</c> to <c>j</c> is larger than zero and for every index <c>k</c> where the
		/// transition from <c>i</c> to <c>k</c> is larger than zero, the transition from <c>k</c> to
		/// <c>i</c> is larger than zero as well.</para>
		/// </remarks>
		public abstract bool IsEssential (int i);

		/// <summary>
		/// Determines whether the given index is inessential (or "transient").
		/// </summary>
		/// <returns><c>true</c> if the given index is inessential (or "transient"); otherwise, <c>false</c>.</returns>
		/// <param name="i">The given index to check for.</param>
		public abstract bool IsInessential (int i);

		/// <summary>
		/// Determines whether the given index is essential in the Cesaro structure.
		/// </summary>
		/// <returns>true</returns>
		/// <paramref name="index"></paramref>
		/// <c>false</c>
		public abstract IEnumerable<int> EssentialIndices ();

		/// <summary>
		/// Gets a list of essential groups of indices.
		/// </summary>
		/// <returns>A list of lists that represent a group of essential indices that only communicate with each other.</returns>
		public abstract IEnumerable<IEnumerable<int>> GetEssentialGroups ();

		/// <summary>
		/// Get the indices of elements that communicate with the given <paramref name="index"/>.
		/// </summary>
		/// <returns>A list of indices with which the given <paramref name="index"/> communicates.</returns>
		/// <param name="index">The given index to look for friends.</param>
		public abstract IEnumerable<int> GetEssentialFriends (int index);

		/// <summary>
		/// Gets a list of inessential self-communicating groups of indices.
		/// </summary>
		/// <returns>A list of lists that represent a group of inessential indices that communicate with each other (and potentially with essential indices as well).</returns>
		/// <remarks>
		/// <para>An inessential self-communicating group is a group of inessential indices that communicate with each other and potentially with
		/// essential indices as well.</para>
		/// </remarks>
		public abstract IEnumerable<IEnumerable<int>> GetInEssentialSelfCommunicatingGroups ();

		/// <summary>
		/// Gets a list of inessential non self-communicating groups of indices.
		/// </summary>
		/// <returns>A list of inessential indices that does not communicate with any index (including itself).</returns>
		/// <remarks>
		/// <para>An inessential non self-communicating group is an index that communicates with no index (including itself).</para>
		/// <para>Since inessential non self-communicating groups are always singletons, we return a list of these indices.</para>
		/// </remarks>
		public abstract IEnumerable<int> GetInEssentialNonSelfCommunicatingGroups ();
		#endregion
		#region ICesaroPermutation implementation
		/// <summary>
		/// Generate a permutation that will swap the cesaro object in the correct order.
		/// </summary>
		/// <returns>A <see cref="IPermutation"/> instance that specifies how a <see cref="T:ICesaroMatrix`1"/> can be swapped
		/// such that the result is a Cesaro structure in the correct order.</returns>
		public abstract IPermutation GetCesaroPermutation ();
		#endregion
		#region ICesaroMatrix implementation
		/// <summary>
		/// Calculate the Cesaro limit, a matrix that represents the average of the powers of the original matrix
		/// where the maximum power goes to infinity.
		/// </summary>
		public abstract void CalculateLimit ();

		/// <summary>
		/// Swaps the indices of the matrix such that the resulting matrix is ordered as is necessary to calculate
		/// the Cesaro limit.
		/// </summary>
		public abstract void SwapCesaro ();

		/// <summary>
		/// Converts this original matrix to its Cesaro limit.
		/// </summary>
		public abstract void ToCesaroLimit ();
		#endregion
		#region IPermutable implementation
		/// <summary>
		/// Swaps the matrix such that the original rows and columns of index <paramref name="i"/> are now the
		/// rows and columns of index <paramref name="j"/> and vice versa.
		/// </summary>
		/// <param name="i">The first index to swap.</param>
		/// <param name="j">The second index to swap.</param>
		public abstract void Swap (int i, int j);

		/// <summary>
		/// Swaps the content of the indices according to the given permutation.
		/// </summary>
		/// <param name="permutation">The given permutation that specifies how the content should be permutated.</param>
		public abstract void Swap (IPermutation permutation);
		#endregion
	}
}

