//
//  CompactTransitionMatrix.cs
//
//  Author:
//       Willem Van Onsem <vanonsem.willem@gmail.com>
//
//  Copyright (c) 2014 Willem Van Onsem
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUtils.Bitwise;
using NUtils.Maths;

namespace CesaroLimit {
	/// <summary>
	/// An implementation of the <see cref="ITransitionMatrix"/> interface. The matrix is represented
	/// as a compact tiled array of <see cref="ulong"/> values. Boosting is done using bitwise operations.
	/// </summary>
	public class CompactTransitionMatrix : ITransitionMatrix {

		#region Fields
		/// <summary>
		/// The number of ulong values per index.
		/// </summary>
		private readonly int stride;
		/// <summary>
		/// The number of indices involved.
		/// </summary>
		private readonly int n;
		/// <summary>
		/// A 1d array of data containing the connectivity between every two indices.
		/// </summary>
		private readonly ulong[] data;
		#endregion
		#region Protected properties
		/// <summary>
		/// Gets a bitmask that masks out the rows of a tile in the last row/column.
		/// </summary>
		/// <value>A bitmask that masks out the rows of a tile in the last row/column.</value>
		public ulong LastMask {
			get {
				return BitUtils.L64ULong >> (0x40 - (this.n << 0x03));
			}
		}
		#endregion
		#region ITransitionMatrix implementation
		/// <summary>
		/// Checks if a transition from <c>i</c> to <c>j</c> is possible.
		/// </summary>
		/// <param name="i">The index from which the transition takes place.</param>
		/// <param name="j">The index to which the transition takes place.</param>
		public bool this [int i, int j] {
			get {
				int ii = i >> 0x03;
				int ji = j >> 0x03;
				ulong e = data [ii * stride + ji];
				return (e & (0x01UL << (((i & 0x07) << 0x03) | (j & 0x07)))) != 0x00;
			}
			set {
				int idx = (i >> 0x03) * stride + (j >> 0x03);
				ulong e = data [idx];
				ulong mask = 0x01UL << (((i & 0x07) << 0x03) | (j & 0x07));
				e &= ~mask;
				if (value) {
					e |= mask;
				}
				data [idx] = e;
			}
		}
		#endregion
		#region ILength implementation
		/// <summary>
		/// Gets the number of subelements.
		/// </summary>
		/// <value>The length.</value>
		public int Length {
			get {
				return this.n;
			}
		}
		#endregion
		/// <summary>
		/// Initializes a new instance of the <see cref="CesaroLimit.CompactTransitionMatrix"/> class with the given number
		/// of indices.
		/// </summary>
		/// <param name="n">The given number of considered indices.</param>
		/// <param name="values">The intial values of the transition matrix.</param>
		public CompactTransitionMatrix (int n, IEnumerable<bool> values) {
			this.n = n;
			int nl = (n + 0x07) >> 0x03;
			this.stride = nl;
			data = BitUtils.TileUlong (values, n).ToArray ();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CesaroLimit.CompactTransitionMatrix"/> class with the given number
		/// of indices.
		/// </summary>
		/// <param name="n">The given number of considered indices.</param>
		/// <param name="values">The intial values of the transition matrix.</param>
		public CompactTransitionMatrix (int n, params bool[] values) : this(n,(IEnumerable<bool>) values) {
		}
		#region ITransitionMatrix implementation
		/// <summary>
		/// Transforms this matrix into a <see cref="ITransitionMatrix"/> that specifies if there exists
		/// an eventual transition from <c>i</c> to <c>j</c>.
		/// </summary>
		/// <remarks>
		/// <para>
		/// An eventual transition is a path from <c>i</c> to <c>j</c> with arbitrary length (such path always has
		/// an upper bound of <c>n</c> with <c>n</c> the number of considered indices).
		/// </para>
		/// </remarks>
		public void Boost () {
			int n = this.n;
			for (int i = 0x01; i <= n; i <<= 0x01) {
				this.BoostSingle ();
			}
		}

		/// <summary>
		/// Transforms this matrix into a <see cref="ITransitionMatrix"/> where the <c>i,j</c>-th element is one
		/// if there is a path from <c>i</c> to <c>j</c> or if there exists a <c>k</c> such that there is a path
		/// from <c>i</c> to <c>j</c> and from <c>j</c> to <c>k</c>.
		/// </summary>
		/// <remarks>
		/// <para>Note that performing this task O(log n) times, will result in a matrix that shows full connectivity. This
		/// is mainly the intention of the <see cref="Boost"/> method.</para>
		/// </remarks>
		public void BoostSingle () {
			int nl = this.stride;
			ulong[] data = this.data;
			int dl = data.Length;
			ulong[] datb = new ulong[dl];
			ulong[] datc = new ulong[dl];
			ulong rw0, rw1, rw2, rw3, rw4, rw5, rw6, rw7, rowk, colk;
			for (int i = 0x00; i < dl; i++) {
				datb [i] = BitUtils.TransposeTile (data [i]);
			}
			int s = nl + 0x01;
			for (int i = 0x00; i < dl; i += s) {
				datb [i] |= BitUtils.ITile;
			}
			int l = 0x00;
			for (int i = 0x00; i < nl; i++) {
				for (int j = 0x00; j < nl; j++) {
					rw0 = 0x00UL;
					rw1 = 0x00UL;
					rw2 = 0x00UL;
					rw3 = 0x00UL;
					rw4 = 0x00UL;
					rw5 = 0x00UL;
					rw6 = 0x00UL;
					rw7 = 0x00UL;
					for (int k = 0x00; k < nl; k++) {
						rowk = data [i * nl + k];
						colk = datb [k * nl + j];

						rw0 |= rowk & BitUtils.Copy8Row (colk, 0x00);
						rw1 |= rowk & BitUtils.Copy8Row (colk, 0x01);
						rw2 |= rowk & BitUtils.Copy8Row (colk, 0x02);
						rw3 |= rowk & BitUtils.Copy8Row (colk, 0x03);
						rw4 |= rowk & BitUtils.Copy8Row (colk, 0x04);
						rw5 |= rowk & BitUtils.Copy8Row (colk, 0x05);
						rw6 |= rowk & BitUtils.Copy8Row (colk, 0x06);
						rw7 |= rowk & BitUtils.Copy8Row (colk, 0x07);
					}
					rw0 = BitUtils.Or8Rows (rw0);
					rw1 = BitUtils.Or8Rows (rw1);
					rw2 = BitUtils.Or8Rows (rw2);
					rw3 = BitUtils.Or8Rows (rw3);
					rw4 = BitUtils.Or8Rows (rw4);
					rw5 = BitUtils.Or8Rows (rw5);
					rw6 = BitUtils.Or8Rows (rw6);
					rw7 = BitUtils.Or8Rows (rw7);
					datc [l++] = BitUtils.Compose8Col (rw0, rw1, rw2, rw3, rw4, rw5, rw6, rw7);
				}
			}
			for (int i = 0x00; i < dl; i++) {
				data [i] = datc [i];
			}
		}

		/// <summary>
		/// Checks whether the two given indices communicate with each other.
		/// </summary>
		/// <param name="i">The first index to check for.</param>
		/// <param name="j">The second index to check for.</param>
		public bool Communicate (int i, int j) {
			return this [i, j] && this [j, i];
		}
		#endregion
		#region ToString method
		/// <summary>
		/// Returns a <see cref="System.String"/> that represents the current <see cref="CesaroLimit.CompactTransitionMatrix"/>.
		/// </summary>
		/// <returns>A <see cref="System.String"/> that represents the current <see cref="CesaroLimit.CompactTransitionMatrix"/>.</returns>
		public override string ToString () {
			StringBuilder sb = new StringBuilder ();
			int n = this.n;
			int nl = this.stride;
			int span = n - (nl << 0x03) + 0x08;
			for (int i = 0x00; i < nl; i++) {
				int e = Math.Min (n - 0x08 * i, 0x08);
				for (int ii = 0x00; ii < e; ii++) {
					for (int j = 0x00; j < nl-0x01; j++) {
						BitUtils.PrintRow (sb, data [i * nl + j], ii);
					}
					BitUtils.PrintRow (sb, data [i * nl + nl - 0x01], ii, span);
					sb.AppendLine ();
				}
			}
			return sb.ToString ();
		}
		#endregion
		#region TileToString method
		/// <summary>
		/// Gets a textual representation of this <see cref="CompactTransitionMatrix"/> instance on tile level.
		/// </summary>
		/// <returns>A string containing an enumeration of the <see cref="CompactTransitionMatrix"/> where
		/// tiles are visually separated.</returns>
		public string TileToString () {
			StringBuilder sb = new StringBuilder ();
			int nl = this.stride;
			sb.Append ('+');
			for (int i = 0x00; i < nl; i++) {
				sb.Append ("--------+");
			}
			sb.AppendLine ();
			string interline = sb.ToString ();
			for (int i = 0x00; i < nl; i++) {
				for (int ii = 0x00; ii < 0x08; ii++) {
					sb.Append ('|');
					for (int j = 0x00; j < nl; j++) {
						BitUtils.PrintRow (sb, data [i * nl + j], ii);
						sb.Append ('|');
					}
					sb.AppendLine ();
				}
				sb.Append (interline);
			}
			return sb.ToString ();
		}
		#endregion
		#region TileCompactToString method
		/// <summary>
		/// Gets a textual representation of the <see cref="CompactTransitionMatrix"/> instance on tile level. The
		/// tiles are represented using hexadecimal values.
		/// </summary>
		/// <returns>A textual representation of the matrix using tiles represented by their hexadecimal
		/// representation.</returns>
		public string TileCompactToString () {
			StringBuilder sb = new StringBuilder ();
			int nl = this.stride;
			sb.Append ('+');
			for (int i = 0x00; i < nl; i++) {
				sb.Append ("----------------+");
			}
			sb.AppendLine ();
			string interline = "\n" + sb.ToString ();
			for (int i = 0x00; i < nl; i++) {
				sb.Append ('|');
				for (int j = 0x00; j < nl; j++) {
					sb.Append (data [i * nl + j].ToString ("X16"));
					sb.Append ('|');
				}
				sb.Append (interline);
			}
			return sb.ToString ();
		}
		#endregion
		#region Equals and GetHashCode methods
		/// <summary>
		/// Determines whether the specified <see cref="System.Object"/> is equal to the current <see cref="CesaroLimit.CompactTransitionMatrix"/>.
		/// </summary>
		/// <param name="obj">The <see cref="System.Object"/> to compare with the current <see cref="CesaroLimit.CompactTransitionMatrix"/>.</param>
		/// <returns><c>true</c> if the specified <see cref="System.Object"/> is equal to the current
		/// <see cref="CesaroLimit.CompactTransitionMatrix"/>; otherwise, <c>false</c>.</returns>
		public override bool Equals (object obj) {
			CompactTransitionMatrix ctm = obj as CompactTransitionMatrix;
			if (ctm == null || this.n != ctm.n) {
				return false;
			}
			ulong[] data = this.data, datb = ctm.data;
			int nd = data.Length;
			for (int i = 0x00; i < nd; i++) {
				if (data [i] != datb [i]) {
					return false;
				}
			}
			return true;
		}

		/// <summary>
		/// Serves as a hash function for a <see cref="CesaroLimit.CompactTransitionMatrix"/> object.
		/// </summary>
		/// <returns>A hash code for this instance that is suitable for use in hashing algorithms and data structures such as a hash table.</returns>
		public override int GetHashCode () {
			int hc = this.n.GetHashCode ();
			foreach (ulong di in this.data) {
				hc *= 0x03;
				hc ^= di.GetHashCode ();
			}
			return hc;
		}
		#endregion
		#region ITransitionMatrix implementation
		/// <summary>
		/// Calculates a <see cref="IBitVector"/> where all bits are set of the essential indices.
		/// </summary>
		/// <returns>A <see cref="IBitVector"/> where all the bits are set of the essential indices.</returns>
		/// <remarks>
		/// <para>By processing essential indices in batch, one yields a performance gain.</para>
		/// </remarks>
		public IBitVector EssentialIndicesVector () {
			int nl = this.stride, nl1 = nl - 0x01, md;
			int n = this.n;
			ulong[] vec = new ulong[(n + 0x3f) >> 0x06];
			int j = 0x00;
			ulong res, lm = this.LastMask;
			ulong[] dt = this.data;
			for (int cii = 0x00; cii < nl;) {
				int ci = cii;
				int ri = nl * ci;
				int rI = ri + nl;
				ulong mul = BitUtils.I8S8L1ULong;
				if (cii == nl1) {
					mul &= lm;
				}
				for (; mul != 0x00 && ri < rI; ri++, ci += nl) {
					mul &= BitUtils.AndCompress8Rows ((~dt [ri]) | BitUtils.TransposeTile (dt [ci]));
				}
				res = BitUtils.TileSerialColumn (mul);
				md = cii & 0x03;
				cii++;
				vec [j] |= res << (md << 0x03);
				j += (cii >> 0x02) & 0x01;
			}
			return new CompactBitVector (n, vec);
		}

		/// <summary>
		/// Calculates a <see cref="IBitVector"/> where all bits are set of the non-communicating indices.
		/// </summary>
		/// <returns>A <see cref="IBitVector"/> where all the bits are set of the non-communicating indices.</returns>
		/// <remarks>
		/// <para>By processing non-communicating indices in batch, one yields a performance gain.</para>
		/// </remarks>
		public IBitVector NonCommunicatingIndicesVector () {
			int nl = this.stride, nl1 = nl - 0x01, md;
			int n = this.n;
			ulong[] vec = new ulong[(n + 0x3f) >> 0x06];
			int j = 0x00;
			ulong res;
			ulong[] dt = this.data;
			for (int cii = 0x00; cii < nl;) {
				int ci = cii;
				int ri = nl * ci;
				int rI = ri + nl;
				ulong mul = BitUtils.I8S8L1ULong;
				if (cii == nl1) {
					mul &= this.LastMask;
				}
				for (; mul != 0x00 && ri < rI; ri++, ci += nl) {
					mul &= BitUtils.AndCompress8Rows (~(dt [ri] & BitUtils.TransposeTile (dt [ci])));
				}
				res = BitUtils.TileSerialColumn (mul);
				md = cii & 0x07;
				cii++;
				vec [j] |= res << (md << 0x03);
				j += (cii >> 0x03) & 0x01;
			}
			return new CompactBitVector (n, vec);
		}

		/// <summary>
		/// Get the indices of elements that communicate with the given <paramref name="index"/>.
		/// </summary>
		/// <returns>A list of indices with which the given <paramref name="index"/> communicates.</returns>
		/// <param name="index">The given index to look for friends.</param>
		/// <param name="frm">An optional parameter that specifies the index from which essential friend
		/// detection begins. This is usefull to optimize essential group detection.</param>
		/// <remarks>
		/// <para>By default, the essential friend detection starts at index zero (<c>0</c>).</para>
		/// </remarks>
		public IBitVector GetEssentialFriends (int index, int frm = 0x00) {
			int nl = this.stride, md;
			int n = this.n;
			ulong[] vec = new ulong[(n + 0x3f) >> 0x06];
			ulong res;
			ulong[] dt = this.data;
			int ci = index >> 0x06;
			int shr = (index & 0x07) << 0x03;
			int ri = nl * ci;
			int rI = ri + nl;
			int frm8 = frm >> 0x03;
			ri += frm8;
			ci += frm8 * nl;
			int l = frm8 & 0x03, j = frm8 >> 0x03;
			for (; ri < rI; ri++, ci += nl) {
				res = ((dt [ri] & BitUtils.TransposeTile (dt [ci])) >> shr) & 0xFFUL;
				md = l & 0x07;
				l++;
				vec [j] |= res << (md << 0x03);
				j += (l >> 0x03) & 0x01;
			}
			return new CompactBitVector (n, vec);
		}

		/// <summary>
		/// Generate a permutation that will generate a matrix in the correct order 
		/// </summary>
		/// <returns>A <see cref="IPermutation"/> instance that specifies how a <see cref="T:ICesaroMatrix`1"/> can be swapped
		/// such that the result is a Cesaro matrix.</returns>
		public IPermutation GetCesaroPermutation () {
			int n = this.n, k = 0x00;
			CompactBitVector cbv = this.EssentialIndicesVector () as CompactBitVector, cbw;
			int low = cbv.GetLowest ();
			int[] idx = new int[n];
			while (low != -0x01) {
				CompactBitVector cur = this.GetEssentialFriends (low, low) as CompactBitVector;
				cur.AndLocal (cbv);
				cbv.Remove (cur);
				foreach (int i in cur) {
					idx [i] = k++;
				}
				low = cbv.GetLowest (low + 0x01);
			}
			cbv = this.EssentialIndicesVector () as CompactBitVector;
			cbw = this.NonCommunicatingIndicesVector () as CompactBitVector;
			cbv.OrLocal (cbw);
			cbv.NotLocal ();
			low = cbv.GetLowest ();
			while (low != -0x01) {
				CompactBitVector cur = this.GetEssentialFriends (low, low) as CompactBitVector;
				cur.AndLocal (cbv);
				foreach (int i in cur) {
					idx [i] = k++;
				}
				cbv.Remove (cur);
				low = cbv.GetLowest (low + 0x01);
			}
			foreach (int i in cbw) {
				idx [i] = k++;
			}
			return new ExplicitPermutation (idx);
		}
		#endregion
		#region ICesaro implementation
		/// <summary>
		/// Checks whether the given index <paramref name="i"/> is inessential (also called "transient").
		/// </summary>
		/// <param name="i">The index to check for.</param>
		/// <returns>True if the given index is inessential, false otherwise.</returns>
		public bool IsInessential (int i) {
			return !this.IsEssential (i);
		}

		/// <summary>
		/// Checks whether the given index <paramref name="i"/> is essential.
		/// </summary>
		/// <param name="i">The index to check for.</param>
		/// <returns>True if the given index is essential, false otherwise.</returns>
		public bool IsEssential (int i) {
			int ci = (i >> 0x03);
			int ii = (i & 0x07) << 0x03;
			int nl = this.stride;
			int ri = nl * ci;
			int rI = ri + nl;
			ulong[] dt = this.data;
			ulong mask = 0xFFUL << ii;
			ulong mul = mask;
			for (; mul == mask && ri < rI; ri++, ci += nl) {
				mul &= (~dt [ri]) | BitUtils.TransposeTile (dt [ci]);
			}
			return (mul == mask);
		}

		/// <summary>
		/// Determines whether the given index is essential in the Cesaro structure.
		/// </summary>
		/// <returns>true</returns>
		/// <paramref name="index"></paramref>
		/// <c>false</c>
		public IEnumerable<int> EssentialIndices () {
			int nl = this.stride, nl1 = nl - 0x01, ci, ri, rI, offset;
			ulong[] dt = this.data;
			ulong mul;
			for (int cii = 0x00; cii < nl; cii++) {
				ci = cii;
				ri = nl * ci;
				rI = ri + nl;
				mul = BitUtils.I8S8L1ULong;
				if (cii == nl1) {
					mul &= this.LastMask;
				}
				for (; mul != 0x00 && ri < rI; ri++, ci += nl) {
					mul &= BitUtils.AndCompress8Rows ((~dt [ri]) | BitUtils.TransposeTile (dt [ci]));
				}
				if (mul != 0x00) {
					offset = cii << 0x03;
					do {
						if ((mul & 0x01) != 0x00) {
							yield return offset;
						}
						mul >>= 0x08;
						offset++;
					} while(mul != 0x00);
				}
			}
		}

		/// <summary>
		/// Gets a list of essential groups of indices.
		/// </summary>
		/// <returns>A list of lists that represent a group of essential indices that only communicate with each other.</returns>
		/// <remarks>
		/// <para>Modifying the bit vector is not allowed, the instance is used later to emit the next results.</para>
		/// </remarks>
		public IEnumerable<IEnumerable<int>> GetEssentialGroups () {
			CompactBitVector cbv = this.EssentialIndicesVector () as CompactBitVector;
			int low = cbv.GetLowest ();
			while (low != -0x01) {
				CompactBitVector cur = this.GetEssentialFriends (low, low) as CompactBitVector;
				cur.AndLocal (cbv);
				cbv.Remove (cur);
				yield return cur;
				low = cbv.GetLowest (low + 0x01);
			}
		}

		/// <summary>
		/// Get the indices of elements that communicate with the given <paramref name="index"/>.
		/// </summary>
		/// <returns>A list of indices with which the given <paramref name="index"/> communicates.</returns>
		/// <param name="index">The given index to look for friends.</param>
		IEnumerable<int> ICesaro<int>.GetEssentialFriends (int index) {
			return this.GetEssentialFriends (index);
		}

		/// <summary>
		/// Gets a list of inessential self-communicating groups of indices.
		/// </summary>
		/// <returns>A list of lists that represent a group of inessential indices that communicate with each other (and potentially
		/// with essential indices as well).</returns>
		/// <remarks>
		/// <para>An inessential self-communicating group is a group of inessential indices that communicate with each other and potentially with
		/// essential indices as well.</para>
		/// </remarks>
		public IEnumerable<IEnumerable<int>> GetInEssentialSelfCommunicatingGroups () {
			CompactBitVector cbv = this.EssentialIndicesVector () as CompactBitVector;
			cbv.OrLocal (this.NonCommunicatingIndicesVector () as CompactBitVector);
			cbv.NotLocal ();
			int low = cbv.GetLowest ();
			while (low != -0x01) {
				CompactBitVector cur = this.GetEssentialFriends (low, low) as CompactBitVector;
				cur.AndLocal (cbv);
				yield return cur;
				cbv.Remove (cur);
				low = cbv.GetLowest (low + 0x01);
			}
		}

		/// <summary>
		/// Gets a list of inessential non self-communicating groups of indices.
		/// </summary>
		/// <returns>A list of inessential indices that does not communicate with any index (including itself).</returns>
		/// <remarks>
		/// <para>An inessential non self-communicating group is an index that communicates with no index (including itself).</para>
		/// <para>Since inessential non self-communicating groups are always singletons, we return a list of these indices.</para>
		/// </remarks>
		public IEnumerable<int> GetInEssentialNonSelfCommunicatingGroups () {
			return this.NonCommunicatingIndicesVector ();
		}
		#endregion
		#region IPermutable implementation
		/// <summary>
		/// Swaps the content of the indices according to the given permutation.
		/// </summary>
		/// <param name="permutation">The given permutation that specifies how the content should be permutated.</param>
		public void Swap (IPermutation permutation) {
			throw new NotImplementedException ();
		}

		/// <summary>
		/// Swaps the content associated with the two given indices.
		/// </summary>
		/// <param name="i">The first index to swap.</param>
		/// <param name="j">The second index to swap.</param>
		public void Swap (int i, int j) {
			throw new NotImplementedException ();
		}
		#endregion
	}
}