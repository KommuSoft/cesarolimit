//
//  CesaroMatrixDouble.cs
//
//  Author:
//       Willem Van Onsem <vanonsem.willem@gmail.com>
//
//  Copyright (c) 2014 Willem Van Onsem
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUtils.Maths;

namespace CesaroLimit {
	/// <summary>
	/// A matrix class that describes a two dimensionally ordered array. Several properties regarding the Cesaro limit
	/// can be checked.
	/// </summary>
	public class CesaroMatrixDouble : CesaroMatrixBase<double> {

		#region Fields
		/// <summary>
		/// An array containing the data of the matrix. For performance reasons, the data is stored in a
		/// one-dimensional array.
		/// </summary>
		private readonly double[] data;
		/// <summary>
		/// A boosted compact transition matrix.
		/// </summary>
		private readonly CompactTransitionMatrix boostedMatrix;
		#endregion
		#region implemented abstract members of CesaroMatrixBase
		/// <summary>
		/// Gets the internally stored data.
		/// </summary>
		/// <value>The internally stored data.</value>
		protected override double[] Data {
			get {
				return this.data;
			}
		}

		/// <summary>
		/// Gets a transition matrix that is boosted. Boosted matrices
		/// are used to calculate which indices that communicate with each
		/// other.
		/// </summary>
		/// <value>The boosted transition matrix.</value>
		protected CompactTransitionMatrix BoostedTransitionMatrix {
			get {
				return this.boostedMatrix;
			}
		}
		#endregion
		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="CesaroMatrixDouble"/> class.
		/// </summary>
		public CesaroMatrixDouble (params double[] values) : base((int) Math.Sqrt (values.Length)) {//TODO: fix
			this.data = (double[])values.Clone ();
			this.boostedMatrix = new CompactTransitionMatrix (this.Length, this.data.Select (x => x > 0.0d));
			this.boostedMatrix.Boost ();
		}
		#endregion
		#region implemented abstract members of CesaroMatrixBase
		/// <summary>
		/// Checks if index <paramref name="i"/> communicates with <paramref name="j"/>.
		/// </summary>
		/// <param name="i">The first given index to check for.</param>
		/// <param name="j">The second given index to check for.</param>
		/// <returns><c>true</c> if index <paramref name="i"/> communicates with index <paramref name="j"/>, otherwise
		/// <c>false</c>.</returns>
		/// <remarks>
		/// <para>An index <paramref name="i"/> communicates with <paramref name="j"/> if the probability of
		/// transition from <paramref name="i"/> to <paramref name="j"/> and the probability of transition from
		/// <paramref name="j"/> to <paramref name="i"/> are both larger than zero.</para>
		/// </remarks>
		public override bool Communicate (int i, int j) {
			return this.boostedMatrix.Communicate (i, j);
		}

		/// <summary>
		/// Checks if one can reach index <paramref name="j"/> from index <paramref name="i"/>, in other
		/// words whether there is a probabilistic path from <paramref name="i"/> to <paramref name="j"/>.
		/// </summary>
		/// <param name="i">The initial index.</param>
		/// <param name="j">The target index.</param>
		public override bool Path (int i, int j) {
			return this.boostedMatrix [i, j];
		}

		/// <summary>
		/// Determines whether the given index is essential.
		/// </summary>
		/// <returns><c>true</c> if the given index is essential; otherwise, <c>false</c>.</returns>
		/// <param name="i">The given index to check for.</param>
		/// <remarks>
		/// <para>An index <c>i</c> is essential if there is at least one index <c>j</c> such that the
		/// transition from <c>i</c> to <c>j</c> is larger than zero and for every index <c>k</c> where the
		/// transition from <c>i</c> to <c>k</c> is larger than zero, the transition from <c>k</c> to
		/// <c>i</c> is larger than zero as well.</para>
		/// </remarks>
		public override unsafe bool IsEssential (int i) {
			return this.boostedMatrix.IsEssential (i);
		}

		/// <summary>
		/// Determines whether the given index is inessential (or "transient").
		/// </summary>
		/// <returns><c>true</c> if the given index is inessential; otherwise, <c>false</c>.</returns>
		/// <param name="i">The given index to check for.</param>
		public override unsafe bool IsInessential (int i) {
			return this.boostedMatrix.IsInessential (i);
		}

		/// <summary>
		/// Determines whether the given index is essential in the Cesaro structure.
		/// </summary>
		/// <returns>true</returns>
		/// <paramref name="index"></paramref>
		/// <c>false</c>
		public override IEnumerable<int> EssentialIndices () {
			return this.boostedMatrix.EssentialIndices ();
		}

		/// <summary>
		/// Get the indices of elements that communicate with the given <paramref name="index"/>.
		/// </summary>
		/// <returns>A list of indices with which the given <paramref name="index"/> communicates.</returns>
		/// <param name="index">The given index to look for friends.</param>
		public override IEnumerable<int> GetEssentialFriends (int index) {
			throw new NotImplementedException ();
		}

		/// <summary>
		/// Gets a list of essential groups of indices.
		/// </summary>
		/// <returns>A list of lists that represent a group of essential indices that only communicate with each other.</returns>
		public override IEnumerable<IEnumerable<int>> GetEssentialGroups () {
			return this.boostedMatrix.GetEssentialGroups ();
		}

		/// <summary>
		/// Gets a list of inessential self-communicating groups of indices.
		/// </summary>
		/// <returns>A list of lists that represent a group of inessential indices that communicate with each other (and potentially with essential indices as well).</returns>
		/// <remarks>
		/// <para>An inessential self-communicating group is a group of inessential indices that communicate with each other and potentially with
		/// essential indices as well.</para>
		/// </remarks>
		public override IEnumerable<IEnumerable<int>> GetInEssentialSelfCommunicatingGroups () {
			return this.boostedMatrix.GetInEssentialSelfCommunicatingGroups ();
		}

		/// <summary>
		/// Gets a list of inessential non self-communicating groups of indices.
		/// </summary>
		/// <returns>A list of inessential indices that does not communicate with any index (including itself).</returns>
		/// <remarks>
		/// <para>An inessential non self-communicating group is an index that communicates with no index (including itself).</para>
		/// <para>Since inessential non self-communicating groups are always singletons, we return a list of these indices.</para>
		/// </remarks>
		public override IEnumerable<int> GetInEssentialNonSelfCommunicatingGroups () {
			return this.boostedMatrix.GetInEssentialNonSelfCommunicatingGroups ();
		}

		/// <summary>
		/// Swaps the matrix such that the original rows and columns of index <paramref name="i"/> are now the
		/// rows and columns of index <paramref name="j"/> and vice versa.
		/// </summary>
		/// <param name="i">The first index to swap.</param>
		/// <param name="j">The second index to swap.</param>
		public override unsafe void Swap (int i, int j) {
			this.boostedMatrix.Swap (i, j);
			int n = this.Length;
			double[] dat = this.Data;
			fixed(double* data0 = &dat[0x00]) {
				double* dataki = data0 + i;
				double* datakj = data0 + j;
				double* datan = data0 + n * n;
				double tmp;
				for (; dataki < datan; dataki += n, datakj += n) {
					tmp = *dataki;
					*dataki = *datakj;
					*datakj = tmp;
				}
				double* dataik = data0 + n * i;
				double* datain = dataik + n;
				double* datajk = data0 + n * j;
				for (; dataik < datain; dataik++, datajk++) {
					tmp = *dataik;
					*dataik = *datajk;
					*datajk = tmp;
				}
			}
		}

		/// <summary>
		/// Swaps the content of the indices according to the given permutation.
		/// </summary>
		/// <param name="permutation">The given permutation that specifies how the content should be permutated.</param>
		public override void Swap (IPermutation permutation) {
			this.boostedMatrix.Swap (permutation);
			//TODO:
		}

		/// <summary>
		/// Swaps the indices of the matrix such that the resulting matrix is ordered as is necessary to calculate
		/// the Cesaro limit.
		/// </summary>
		public override void SwapCesaro () {
			/*for(int ) {

			}*/
		}

		/// <summary>
		/// Converts this original matrix to its Cesaro limit.
		/// </summary>
		public override void ToCesaroLimit () {
			throw new NotImplementedException ();
		}

		/// <summary>
		/// Generate a permutation that will swap the cesaro object in the correct order.
		/// </summary>
		/// <returns>A <see cref="IPermutation"/> instance that specifies how a <see cref="T:ICesaroMatrix`1"/> can be swapped
		/// such that the result is a Cesaro structure in the correct order.</returns>
		public override IPermutation GetCesaroPermutation () {
			return this.boostedMatrix.GetCesaroPermutation ();
		}

		/// <summary>
		/// Calculate the Cesaro limit, a matrix that represents the average of the powers of the original matrix
		/// where the maximum power goes to infinity.
		/// </summary>
		public override void CalculateLimit () {
			throw new NotImplementedException ();
		}
		#endregion
		#region ToString method
		/// <summary>
		/// Returns a <see cref="System.String"/> that represents the current <see cref="CesaroLimit.CesaroMatrixDouble"/>.
		/// </summary>
		/// <returns>A <see cref="System.String"/> that represents the current <see cref="CesaroLimit.CesaroMatrixDouble"/>.</returns>
		public override string ToString () {
			StringBuilder sb = new StringBuilder ();
			double[] d = this.data;
			int n = this.Length, n1 = n - 0x01;
			int k = 0x00;
			for (int i = 0x00; i < n; i++) {
				for (int j = 0x00; j < n1; j++) {
					sb.Append (d [k++].ToString ("F3"));
					sb.Append (' ');
				}
				sb.Append (d [k++].ToString ("F3"));
				sb.AppendLine ();
			}
			return sb.ToString ();
		}
		#endregion
	}
}