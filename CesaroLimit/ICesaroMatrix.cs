//
//  ICesaroMatrix.cs
//
//  Author:
//       Willem Van Onsem <vanonsem.willem@gmail.com>
//
//  Copyright (c) 2014 Willem Van Onsem
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using NUtils.Maths;

namespace CesaroLimit {
	/// <summary>
	/// An interface describing a matrix that supports typical operations on a Cesaro matrix. Used for interfacing
	/// purposes.
	/// </summary>
	/// <typeparam name='TElement'>
	/// The type of elements stored in the <see cref="T:ICesaroMatrix`1"/>. These elements should have an order relation together
	/// with a zero-element.
	/// </typeparam>
	public interface ICesaroMatrix<TElement> : IPermutable, ICesaroPermutation where TElement : IComparable<TElement> {

		/// <summary>
		/// Gets the element at the given index of the matrix.
		/// </summary>
		/// <param name="index">The given index.</param>
		TElement this [int index] {
			get;
		}

		/// <summary>
		/// Gets the element at the given row and column of the matrix.
		/// </summary>
		/// <param name="row">The given row.</param>
		/// <param name="column">The given column.</param>
		TElement this [int row, int column] {
			get;
		}

		/// <summary>
		/// Checks if one can reach index <paramref name="j"/> from index <paramref name="i"/>, in other
		/// words whether there is a probabilistic path from <paramref name="i"/> to <paramref name="j"/>.
		/// </summary>
		/// <param name="i">The initial index.</param>
		/// <param name="j">The target index.</param>
		bool Path (int i, int j);

		/// <summary>
		/// Swaps the indices of the matrix such that the resulting matrix is ordered as is necessary to calculate
		/// the Cesaro limit.
		/// </summary>
		void SwapCesaro ();

		/// <summary>
		/// Converts this original matrix to its Cesaro limit.
		/// </summary>
		void ToCesaroLimit ();

		/// <summary>
		/// Calculate the Cesaro limit, a matrix that represents the average of the powers of the original matrix
		/// where the maximum power goes to infinity.
		/// </summary>
		void CalculateLimit ();
	}
}

