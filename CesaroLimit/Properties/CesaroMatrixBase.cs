using System;

namespace CesaroLimit {
	/// <summary>
	/// A basic generic implementation of the <see cref="T:ICesaroMatrix`1"/> interface. This basic implementation mainly
	/// handles operations like swapping etc.
	/// </summary>
	public abstract class CesaroMatrixBase<TElement> : ICesaroMatrix<TElement> {

		private readonly int n;

		/// <summary>
		/// Gets the internally stored data.
		/// </summary>
		/// <value>The internally stored data.</value>
		protected abstract TElement[] Data {
			get;
		}

		/// <summary>
		/// Gets the element at the given index of the matrix.
		/// </summary>
		/// <param name="index">The given index.</param>
		public TElement this [int index] {
			get {
				return this.Data [index];
			}
		}

		/// <summary>
		/// Gets the element at the given row and column of the matrix.
		/// </summary>
		/// <param name="row">The given row.</param>
		/// <param name="column">The given column.</param>
		public TElement this [int row, int column] {
			get {
				return this.Data [row * N + column];
			}
		}

		/// <summary>
		/// Gets the number of elements that are handled by the transition matrix.
		/// </summary>
		/// <value>The number of elements handled by the transition matrix.</value>
		public int N {
			get {
				return this.n;
			}
		}

		protected CesaroMatrixBase (int n) {
			this.n = n;
		}
		#region ICesaroMatrix implementation
		/// <summary>
		/// Checks if index <paramref name="i"/> communicates with <paramref name="j"/>.
		/// </summary>
		/// <param name="i">The first given index to check for.</param>
		/// <param name="j">The second given index to check for.</param>
		/// <returns><c>true</c> if index <paramref name="i"/> communicates with index <paramref name="j"/>, otherwise
		/// <c>false</c>.</returns>
		/// <remarks>
		/// <para>An index <paramref name="i"/> communicates with <paramref name="j"/> if the probability of
		/// transition from <paramref name="i"/> to <paramref name="j"/> and the probability of transition from
		/// <paramref name="j"/> to <paramref name="i"/> are both larger than zero.</para>
		/// </remarks>
		public abstract bool Communicate (int i, int j);

		/// <summary>
		/// Determines whether the given index is essential.
		/// </summary>
		/// <returns><c>true</c> if the given index is essential; otherwise, <c>false</c>.</returns>
		/// <param name="i">The given index to check for.</param>
		/// <remarks>
		/// <para>An index <c>i</c> is essential if there is at least one index <c>j</c> such that the
		/// transition from <c>i</c> to <c>j</c> is larger than zero and for every index <c>k</c> where the
		/// transition from <c>i</c> to <c>k</c> is larger than zero, the transition from <c>k</c> to
		/// <c>i</c> is larger than zero as well.</para>
		/// </remarks>
		public abstract bool IsEssential (int i);

		/// <summary>
		/// Swaps the matrix such that the original rows and columns of index <paramref name="i"/> are now the
		/// rows and columns of index <paramref name="j"/> and vice versa.
		/// </summary>
		/// <param name="i">The first index to swap.</param>
		/// <param name="j">The second index to swap.</param>
		public abstract void Swap (int i, int j);
		#endregion
	}
}

