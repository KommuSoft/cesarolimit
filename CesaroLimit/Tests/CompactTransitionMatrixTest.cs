//
//  CompactTransitionMatrixTest.cs
//
//  Author:
//       Willem Van Onsem <vanonsem.willem@gmail.com>
//
//  Copyright (c) 2014 Willem Van Onsem
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using NUnit.Framework;
using NUtils.Bitwise;

namespace CesaroLimit {
	[TestFixture()]
	public class CompactTransitionMatrixTest {

		public const string strCTM1 = "00000\n00000\n00000\n00000\n00000\n";
		private static readonly bool[] connCTM2 = new bool[] {
			true, true, false, false, false, false,
			false, false, false, false, false, true,
			false, false, false, true, false, false,
			false, false, false, false, false, false,
			false, false, false, false, false, false,
			true, false, false, false, false, false
		};
		private static readonly bool[] connCTM2B1 = new bool[] {
			true, true, false, false, false, true,
			true, false, false, false, false, true,
			false, false, false, true, false, false,
			false, false, false, false, false, false,
			false, false, false, false, false, false,
			true, true, false, false, false, false
		};
		private static readonly bool[] connCTM3 = new bool[] {
			false, true, false, false, false, false,
			false, false, true, false, false, false,
			false, false, false, true, false, false,
			false, false, false, false, true, false,
			false, false, false, false, false, true,
			true, false, false, false, false, false
		};
		private static readonly bool[] connCTM3B = new bool[] {
			true, true, true, true, true, true,
			true, true, true, true, true, true,
			true, true, true, true, true, true,
			true, true, true, true, true, true,
			true, true, true, true, true, true,
			true, true, true, true, true, true
		};
		private static readonly bool[] connCTM4 = new bool[] {
			false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false,
			true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
			false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false,
			false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false,
			false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false,
			false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false,
			false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
			false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false,
			false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true,
			false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false,
			false, false, false, false, false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false,
			false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
			false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
			false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false,
			false, false, false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false,
			false, false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false,
			false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
			false, false, false, false, false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
			false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, false,
			false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false
		};
		private static readonly bool[] connCTM5 = new bool[] {
			true, false, false, true, false, false, false,
			false, false, true, true, true, false, false,
			false, false, true, false, false, true, false,
			true, false, false, true, false, false, false,
			true, true, false, false, true, false, false,
			false, false, true, false, false, false, false,
			true, false, true, false, true, false, false
		};
		bool[,] commCTM5B = new bool[,] {
			{true, false,false,true, false,false,false},
			{false,true, false,false,true, false,false},
			{false,false,true, false,false,true, false},
			{true, false,false,true, false,false,false},
			{false,true, false,false,true, false,false},
			{false,false,true, false,false,true, false},
			{false,false,false,false,false,false,false}
		};
		public const string strCTM2 = "110000\n000001\n000100\n000000\n000000\n100000\n";
		public const string strCTM3 = "010000\n001000\n000100\n000010\n000001\n100000\n";
		public const string strCTM2B1 = "110001\n100001\n000100\n000000\n000000\n110000\n";
		public const string strCTM3B1 = "011000\n001100\n000110\n000011\n100001\n110000\n";
		public const string strCTM3B2 = "011110\n001111\n100111\n110011\n111001\n111100\n";
		public const string strCTM3B3 = "111111\n111111\n111111\n111111\n111111\n111111\n";
		public const string strCTM3B = "111111\n111111\n111111\n111111\n111111\n111111\n";
		public const string strCTM4 = "00000000000000001000\n10000000000000000000\n00000000000001000000\n00000000000100000000\n00000000000000100000\n00000000010000000000\n00010000000000000000\n00000000001000000000\n00000000000000000001\n00000000000000010000\n00000000000010000000\n01000000000000000000\n00001000000000000000\n00000001000000000000\n00000000100000000000\n00000010000000000000\n00100000000000000000\n00000100000000000000\n00000000000000000100\n00000000000000000010\n";
		public const string strCTM4B1 = "00100000000000001000\n10000000000000001000\n00000001000001000000\n01000000000100000000\n00000000100000100000\n00000000010000010000\n00010000000100000000\n00000000001010000000\n00000000000000000011\n00000010000000010000\n00001000000010000000\n11000000000000000000\n00001000000000100000\n00000001001000000000\n00000000100000000001\n00010010000000000000\n00100000000001000000\n00000100010000000000\n00000100000000000100\n00000000000000000110\n";
		public const string strCTM4B = "11111111111111111111\n11111111111111111111\n11111111111111111111\n11111111111111111111\n11111111111111111111\n11111111111111111111\n11111111111111111111\n11111111111111111111\n11111111111111111111\n11111111111111111111\n11111111111111111111\n11111111111111111111\n11111111111111111111\n11111111111111111111\n11111111111111111111\n11111111111111111111\n11111111111111111111\n11111111111111111111\n11111111111111111111\n11111111111111111111\n";
		public const string strCTM5B = "1001000\n1111110\n0010010\n1001000\n1111110\n0010010\n1111110\n";
		public const string strCTM5F0 = "1001000";
		public const string strCTM5F1 = "0100100";
		public const string strCTM5F2 = "0010010";
		public const string strCTM5F3 = "1001000";
		public const string strCTM5F4 = "0100100";
		public const string strCTM5F5 = "0010010";
		public const string strCTM5F6 = "0000000";

		[Test()]
		public void TestToString () {
			CompactTransitionMatrix ctm;
			ctm = GenerateMatrix1 ();
			Assert.AreEqual (strCTM1, ctm.ToString ());
			ctm = GenerateMatrix2 ();
			Assert.AreEqual (strCTM2, ctm.ToString ());
			ctm = GenerateMatrix3 ();
			Assert.AreEqual (strCTM3, ctm.ToString ());
			ctm = GenerateMatrix4 ();
			Assert.AreEqual (strCTM4, ctm.ToString ());
		}

		[Test()]
		public void TestBoost () {
			CompactTransitionMatrix ctm;
			ctm = GenerateMatrix3 ();
			ctm.Boost ();
			Assert.AreEqual (strCTM3B, ctm.ToString ());
			ctm = GenerateMatrix4 ();
			ctm.Boost ();
			Assert.AreEqual (strCTM4B, ctm.ToString ());
			ctm = GenerateMatrix5 ();
			ctm.Boost ();
			Assert.AreEqual (strCTM5B, ctm.ToString ());
		}

		[Test()]
		public void TestBoostSingle () {
			CompactTransitionMatrix ctm;
			ctm = GenerateMatrix2 ();
			ctm.BoostSingle ();
			Assert.AreEqual (strCTM2B1, ctm.ToString ());
			ctm = GenerateMatrix3 ();
			ctm.BoostSingle ();
			Assert.AreEqual (strCTM3B1, ctm.ToString ());
			ctm.BoostSingle ();
			Assert.AreEqual (strCTM3B2, ctm.ToString ());
			ctm.BoostSingle ();
			Assert.AreEqual (strCTM3B3, ctm.ToString ());
			ctm = GenerateMatrix4 ();
			ctm.BoostSingle ();
			Assert.AreEqual (strCTM4B1, ctm.ToString ());
		}

		[Test()]
		public void TestGetEssentialFriends () {
			CompactTransitionMatrix ctm;
			IBitVector ibv;
			ctm = GenerateMatrix5 ();
			ctm.Boost ();
			ibv = ctm.GetEssentialFriends (0x00);
			Assert.AreEqual (strCTM5F0, ibv.ToString ());
			ibv = ctm.GetEssentialFriends (0x01);
			Assert.AreEqual (strCTM5F1, ibv.ToString ());
			ibv = ctm.GetEssentialFriends (0x02);
			Assert.AreEqual (strCTM5F2, ibv.ToString ());
			ibv = ctm.GetEssentialFriends (0x03);
			Assert.AreEqual (strCTM5F3, ibv.ToString ());
			ibv = ctm.GetEssentialFriends (0x04);
			Assert.AreEqual (strCTM5F4, ibv.ToString ());
			ibv = ctm.GetEssentialFriends (0x05);
			Assert.AreEqual (strCTM5F5, ibv.ToString ());
			ibv = ctm.GetEssentialFriends (0x06);
			Assert.AreEqual (strCTM5F6, ibv.ToString ());
		}

		public static CompactTransitionMatrix GenerateMatrix1 () {
			return new CompactTransitionMatrix (0x05);
		}

		public static CompactTransitionMatrix GenerateMatrix2 () {
			return new CompactTransitionMatrix (0x06, connCTM2);
		}

		public static CompactTransitionMatrix GenerateMatrix3 () {
			return new CompactTransitionMatrix (0x06, connCTM3);
		}

		public static CompactTransitionMatrix GenerateMatrix4 () {
			return new CompactTransitionMatrix (0x14, connCTM4);
		}

		public static CompactTransitionMatrix GenerateMatrix5 () {
			return new CompactTransitionMatrix (0x07, connCTM5);
		}
	}
}

