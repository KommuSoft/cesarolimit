//
//  CesaroMatrixDoubleTest.cs
//
//  Author:
//       Willem Van Onsem <vanonsem.willem@gmail.com>
//
//  Copyright (c) 2014 Willem Van Onsem
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using NUnit.Framework;
using System.Linq;
using NUtils.Maths;
using System;

namespace CesaroLimit {
	[TestFixture()]
	public class CesaroMatrixDoubleTest {

		public const double f01 = 0.0d;
		public const double f11 = 1.0d;
		public const double f12 = 1.0d / 2.0d;
		public const double f13 = 1.0d / 3.0d;
		public const double f23 = 2.0d / 3.0d;
		public const double f14 = 1.0d / 4.0d;
		public const double f34 = 3.0d / 4.0d;
		public const double f15 = 1.0d / 5.0d;
		public const double f25 = 2.0d / 5.0d;
		public const double f45 = 4.0d / 5.0d;
		public const double f16 = 1.0d / 6.0d;
		public const double f56 = 5.0d / 6.0d;
		public const double f17 = 1.0d / 7.0d;
		public const double f27 = 2.0d / 7.0d;
		public const double f47 = 4.0d / 7.0d;

		[Test()]
		public void TestConstructor1 () {
			CesaroMatrixDouble pmatrix = new CesaroMatrixDouble (new double[] {
				f12, f01, f01, f12, f01, f01, f01,
				f01, f01, f47, f27, f17, f01, f01,
				f01, f01, f13, f01, f01, f23, f01,
				f14, f01, f01, f34, f01, f01, f01,
				f25, f25, f01, f01, f15, f01, f01,
				f01, f01, f11, f01, f01, f01, f01,
				f14, f01, f12, f01, f14, f01, f01
			});
			bool[,] ppath = new bool[,] {
				{true, false,false,true, false,false,false},
				{true, true, true, true, true, true, false},
				{false,false,true, false,false,true, false},
				{true, false,false,true, false,false,false},
				{true, true, true, true, true, true, false},
				{false,false,true, false,false,true, false},
				{true, true, true, true, true, true, false}
			};
			bool[,] pcomm = new bool[,] {
				{true, false,false,true, false,false,false},
				{false,true, false,false,true, false,false},
				{false,false,true, false,false,true, false},
				{true, false,false,true, false,false,false},
				{false,true, false,false,true, false,false},
				{false,false,true, false,false,true, false},
				{false,false,false,false,false,false,false}
			};
			bool[] pesss = new bool[] {
				true, false, true, true, false, true, false
			};
			for (int i = 0x00; i < 0x07; i++) {
				Assert.AreEqual (pesss [i], pmatrix.IsEssential (i));
				for (int j = 0x00; j < 0x07; j++) {
					Assert.AreEqual (ppath [i, j], pmatrix.Path (i, j));
					Assert.AreEqual (pcomm [i, j], pmatrix.Communicate (i, j));
				}
			}
			int[] ei = pmatrix.EssentialIndices ().ToArray ();
			Assert.AreEqual (0x04, ei.Length);
			Assert.AreEqual (0x00, ei [0x00]);
			Assert.AreEqual (0x02, ei [0x01]);
			Assert.AreEqual (0x03, ei [0x02]);
			Assert.AreEqual (0x05, ei [0x03]);
			int[][] eg = pmatrix.GetEssentialGroups ().Select (x => x.ToArray ()).ToArray ();
			Assert.AreEqual (0x02, eg.Length);
			Assert.AreEqual (0x02, eg [0x00].Length);
			Assert.AreEqual (0x00, eg [0x00] [0x00]);
			Assert.AreEqual (0x03, eg [0x00] [0x01]);
			Assert.AreEqual (0x02, eg [0x01].Length);
			Assert.AreEqual (0x02, eg [0x01] [0x00]);
			Assert.AreEqual (0x05, eg [0x01] [0x01]);
			int[][] ic = pmatrix.GetInEssentialSelfCommunicatingGroups ().Select (x => x.ToArray ()).ToArray ();
			Assert.AreEqual (0x01, ic.Length);
			Assert.AreEqual (0x02, ic [0x00].Length);
			Assert.AreEqual (0x01, ic [0x00] [0x00]);
			Assert.AreEqual (0x04, ic [0x00] [0x01]);
			int[] id = pmatrix.GetInEssentialNonSelfCommunicatingGroups ().ToArray ();
			Assert.AreEqual (0x01, id.Length);
			Assert.AreEqual (0x06, id [0x00]);
			IPermutation perm = pmatrix.GetCesaroPermutation ();
			Assert.AreEqual (0x00, perm [0x00]);
			Assert.AreEqual (0x04, perm [0x01]);
			Assert.AreEqual (0x02, perm [0x02]);
			Assert.AreEqual (0x01, perm [0x03]);
			Assert.AreEqual (0x05, perm [0x04]);
			Assert.AreEqual (0x03, perm [0x05]);
			Assert.AreEqual (0x06, perm [0x06]);
		}

		[Test()]
		public void TestConstructor2 () {
			CesaroMatrixDouble qmatrix = new CesaroMatrixDouble (new double[] {
				f13, f01, f01, f23, f01, f01, f01,
				f01, f01, f27, f17, f47, f01, f01,
				f01, f01, f15, f01, f01, f45, f01,
				f16, f01, f01, f56, f01, f01, f01,
				f15, f25, f01, f01, f25, f01, f01,
				f01, f01, f11, f01, f01, f01, f01,
				f14, f01, f14, f01, f12, f01, f01
			});
			bool[,] qpath = new bool[,] {
				{true, false,false,true, false,false,false},
				{true, true, true, true, true, true, false},
				{false,false,true, false,false,true, false},
				{true, false,false,true, false,false,false},
				{true, true, true, true, true, true, false},
				{false,false,true, false,false,true, false},
				{true, true, true, true, true, true, false}
			};
			bool[,] qcomm = new bool[,] {
				{true, false,false,true, false,false,false},
				{false,true, false,false,true, false,false},
				{false,false,true, false,false,true, false},
				{true, false,false,true, false,false,false},
				{false,true, false,false,true, false,false},
				{false,false,true, false,false,true, false},
				{false,false,false,false,false,false,false}
			};
			bool[] qesss = new bool[] {
				true, false, true, true, false, true, false
			};
			for (int i = 0x00; i < 0x07; i++) {
				Assert.AreEqual (qesss [i], qmatrix.IsEssential (i));
				for (int j = 0x00; j < 0x07; j++) {
					Assert.AreEqual (qpath [i, j], qmatrix.Path (i, j));
					Assert.AreEqual (qcomm [i, j], qmatrix.Communicate (i, j));
				}
			}
			int[] ei = qmatrix.EssentialIndices ().ToArray ();
			Assert.AreEqual (0x04, ei.Length);
			Assert.AreEqual (0x00, ei [0x00]);
			Assert.AreEqual (0x02, ei [0x01]);
			Assert.AreEqual (0x03, ei [0x02]);
			Assert.AreEqual (0x05, ei [0x03]);
			int[][] eg = qmatrix.GetEssentialGroups ().Select (x => x.ToArray ()).ToArray ();
			Assert.AreEqual (0x02, eg.Length);
			Assert.AreEqual (0x02, eg [0x00].Length);
			Assert.AreEqual (0x00, eg [0x00] [0x00]);
			Assert.AreEqual (0x03, eg [0x00] [0x01]);
			Assert.AreEqual (0x02, eg [0x01].Length);
			Assert.AreEqual (0x02, eg [0x01] [0x00]);
			Assert.AreEqual (0x05, eg [0x01] [0x01]);
			int[][] ic = qmatrix.GetInEssentialSelfCommunicatingGroups ().Select (x => x.ToArray ()).ToArray ();
			Assert.AreEqual (0x01, ic.Length);
			Assert.AreEqual (0x02, ic [0x00].Length);
			Assert.AreEqual (0x01, ic [0x00] [0x00]);
			Assert.AreEqual (0x04, ic [0x00] [0x01]);
			int[] id = qmatrix.GetInEssentialNonSelfCommunicatingGroups ().ToArray ();
			Assert.AreEqual (0x01, id.Length);
			Assert.AreEqual (0x06, id [0x00]);
			IPermutation perm = qmatrix.GetCesaroPermutation ();
			Assert.AreEqual (0x00, perm [0x00]);
			Assert.AreEqual (0x04, perm [0x01]);
			Assert.AreEqual (0x02, perm [0x02]);
			Assert.AreEqual (0x01, perm [0x03]);
			Assert.AreEqual (0x05, perm [0x04]);
			Assert.AreEqual (0x03, perm [0x05]);
			Assert.AreEqual (0x06, perm [0x06]);
		}
	}
}

