cesarolimit
===========

A C# implementation to calculate the Césaro limit of a Matrix. This can be used to calculate the fundamental Kullback-Leibler divergence between two Hidden Markov Models.

Césaro limit
------------

Given a probability transition matrix ![A](http://latex.codecogs.com/gif.latex?%5CinlineA), we wish to compute the ![the limit of n to infinity the average of A^i with i to n](http://latex.codecogs.com/svg.latex?%5Cdisplaystyle%5Clim_%7Bn%5Cto%5Cinfty%7D%5Cdisplaystyle%5Cfrac%7B1%7D%7Bn%7D%5Cdisplaystyle%5Csum_%7Bi%3D1%7D%5E%7Bn%7DA%5Ei).

Césaro proved that such limit always exists and can be done by permutating the matrix such that the matrix is in a canonical form.

Terminology
-----------

We first introduce some terminology. Since the indices of the matrix correspond to objects that are related by probabilities, it makes sense to see the
matrix as a transition matrix between these objects.

We say that two objects ![i](http://latex.codecogs.com/gif.latex?%5Cinlinei) and ![j](http://latex.codecogs.com/gif.latex?%5Cinlinej) *communicate* if the transition element ![A_ij](http://latex.codecogs.com/svg.latex?a_%7Bij%7D) and ![A_ji](http://latex.codecogs.com/svg.latex?a_%7Bji%7D) are both larger than zero.

We say an object ![i](http://latex.codecogs.com/gif.latex?%5Cinlinei) is *inessential* (or *transient*) if there exists an object ![j](http://latex.codecogs.com/gif.latex?%5Cinlinej) such that ![A_ij](http://latex.codecogs.com/svg.latex?a_%7Bij%7D) is larger than zero and ![A_ji](http://latex.codecogs.com/svg.latex?a_%7Bji%7D) is equal to zero, otherwise the element is called *essential*.

An *essential class* is a set of essential objects such that all the elements in the set communicate with each other.

An *inessential class* is a subset of inessential objects. Two subclasses exist:

 - *self-communicating inessential classes*: a set of inessential objects that communicate with each other.
 - *non self-communicating inessential classes*: this is a singleton, the object is inessential and does not communicate with any object (including itself).
 
 A matrix is *irreducable* if its objects form a single essential class: every object communicates with every other index.
 
Permutation
-----------
